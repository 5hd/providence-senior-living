<?php

add_theme_support( 'title-tag' );
add_theme_support( 'post-thumbnails' );

/**
 * Register Custom Navigation Walker
 */
function register_navwalker(){
    require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';
}
add_action( 'after_setup_theme', 'register_navwalker' );

add_filter( 'nav_menu_link_attributes', 'prefix_bs5_dropdown_data_attribute', 20, 3 );
/**
 * Use namespaced data attribute for Bootstrap's dropdown toggles.
 *
 * @param array    $atts HTML attributes applied to the item's `<a>` element.
 * @param WP_Post  $item The current menu item.
 * @param stdClass $args An object of wp_nav_menu() arguments.
 * @return array
 */
function prefix_bs5_dropdown_data_attribute( $atts, $item, $args ) {
    if ( is_a( $args->walker, 'WP_Bootstrap_Navwalker' ) ) {
        if ( array_key_exists( 'data-toggle', $atts ) ) {
            unset( $atts['data-toggle'] );
            $atts['data-bs-toggle'] = 'dropdown';
        }
    }
    return $atts;
}


function fivehd_enqueue_scripts_styles() {
  wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Montserrat:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic","Judson:regular,italic|Judson:regular,italic' );

  wp_enqueue_script( 'font-awesome', 'https://use.fontawesome.com/releases/v5.0.6/js/all.js', array(), false, true );

    wp_enqueue_style( "bootstrap", "https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" );
    wp_enqueue_style( 'swiper', 'https://unpkg.com/swiper@7/swiper-bundle.min.css"' );

    wp_enqueue_script('jQuerycdn', '//ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js', false, false, true );
    wp_enqueue_script( "bootstrap", "https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js", array('jquery') );
    wp_enqueue_script('swiper_js', 'https://unpkg.com/swiper@7/swiper-bundle.min.js', false, '', true );


    wp_enqueue_style( 'fivehdstarter', get_stylesheet_directory_uri() . '/theme.css', array(), filemtime(get_stylesheet_directory() . '/theme.css'));

    wp_enqueue_script( 'fivehdstarter', get_stylesheet_directory_uri() . '/js/theme.js', array('jquery'));
}
add_action( 'wp_enqueue_scripts', 'fivehd_enqueue_scripts_styles' );

function fivehd_register_nav_menus() {
    register_nav_menus( array(
        'main-navigation' => __( 'Primary Menu', 'fivehdstarter' ),
        'footer-1-navigation' => __( 'Footer Menu 1', 'fivehdstarter' ),
        'footer-2-navigation' => __( 'Footer Menu 2', 'fivehdstarter' ),
    ) );
}
add_action( 'after_setup_theme', 'fivehd_register_nav_menus' );

function fivehd_widgets_init() {
    register_sidebar( array(
        'name'          => __( 'Main Sidebar', 'textdomain' ),
        'id'            => 'main-sidebar',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'fivehd_widgets_init' );


// Gutenberg custom stylesheet
add_theme_support('editor-styles');
add_editor_style( 'editor-style.css' ); // make sure path reflects where the file is located


//Customizer Options
function theme_customize_register( $wp_customize ) {

 //Header Section
    $wp_customize->add_section( 'header' , array(
        'title'      => __( 'Header', 'theme' ),
        'priority'   => 30,
    ) );

    //Heading Logo
    $wp_customize->add_setting( 'header-logo' , array(
        'default'   => '',
        'sanitize_callback' => 'esc_url_raw',
        'section'    => 'header',
    ) );

    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'header-logo', array(
        'label'      => __( 'Header Logo', 'theme' ),
        'section'    => 'header',
        'settings'   => 'header-logo',
    ) ) );

    //Footer Section
    $wp_customize->add_section( 'footer' , array(
        'title'      => __( 'Footer', 'theme' ),
        'priority'   => 30,
    ) );

    //Footer Logo
    $wp_customize->add_setting( 'footer-logo' , array(
        'default'   => '',
        'sanitize_callback' => 'esc_url_raw',
        'section'    => 'footer',
    ) );

    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'footer-logo', array(
        'label'      => __( 'Footer Logo', 'theme' ),
        'section'    => 'footer',
        'settings'   => 'footer-logo',
    ) ) );

}
add_action( 'customize_register', 'theme_customize_register' );


function fivehd_custom_image_sizes() {
  add_image_size( 'header-image', 1800, 500, true );
  add_image_size( 'listed-post', 800, 350, true );
}
add_action( 'after_setup_theme', 'fivehd_custom_image_sizes' );