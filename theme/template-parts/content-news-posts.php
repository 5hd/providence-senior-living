   <section class="post-section">
    <div class="container">
    <div class="row">
   <?php

    $args = array(
      'posts_per_page'      => 100,
      'ignore_sticky_posts'   => 1,
      'post_status'         => 'publish',
      'order'         => 'DESC',
      'order_by'        => 'post_date',
    );
    
    $news_posts = new WP_Query( $args );
    if ( $news_posts->have_posts() ): while ( $news_posts->have_posts() ): $news_posts->the_post();
  ?>
                    
<?php get_template_part( 'template-parts/content','listed-posts' ); ?>

                    <?php
    endwhile;
    wp_reset_postdata();
    endif;
  ?>
</div>
</div>
</section>