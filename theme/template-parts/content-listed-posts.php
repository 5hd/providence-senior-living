<div class="listed-post col-md-12 col-lg-6">
	<div class="post-image">
		 <img src="<?php the_post_thumbnail_url("listed-post");?>" />
</div>
	<h2 class="post-title">
<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
</h2>

<?php the_excerpt(); ?>
<a href="<?php the_permalink(); ?>">
<button class="button">
	Read More
</button>
</a>
</div>