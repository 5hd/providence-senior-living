<?php
/**
* Template part for displaying page content in page.php
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/
*
* @package fivehdstarter
*/
?>
<div class="section-hero-agenda">
  <section class="section-sticky" style="will-change: opacity; opacity: 0.89;">
    <div class="page-padding">
      <div id="w-node-b3fbf7a1-5f49-0468-36a9-7e9ac033e993-5d2ee22c" class="agenda-hero-component">
        <div class="section-page-title">
          <h1 class="heading-large"><?php the_title(); ?></h1>
        </div>
      </div>
    </div>
  </section>
  <div class="hero-basic-trigger"></div>
  <section class="section-intro-columns">
    <div class="page-padding">
      <div class="padding-vertical padding-huge">
        <div class="container-large">
          <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <header class="entry-header headline no-image">
              <?php the_title( '<h1 class="entry-title animated fadeIn duration2 eds-on-scroll">', '</h1>' ); ?>
              <div class="breadcrumbs">
                <?php if ( function_exists('yoast_breadcrumb') ) {
                yoast_breadcrumb( '</p><p id=“breadcrumbs”>','</p><p>' );
                }?>
              </div>
              </header><!-- .entry-header -->
              <div class="container small post-content">
                <div class="row">
                  <div class="col-sm-12 col-md-8 content-column">
                    <img src="<?php the_post_thumbnail_url('listed-post');?>" />
                    <div class="entry-content">
                      <?php the_content(); ?>
                      </div><!-- .entry-content -->
                    </div>
                    <div class="col-sm-12 col-md-4 recent-posts">
                      <div class="sticky">
                      <h2>Recent Posts</h2>
                      <?php get_template_part( 'template-parts/content','recent-posts' ); ?>
                    </div>
                    </div>
                  </div>
                </div>
                <?php if ( get_edit_post_link() ) : ?>
                <footer class="entry-footer">
                  <div class="container small">
                    <a href="/our-difference/news/">
                      <button class="button">
                      Back to news
                      </button>
                    </a>
                  </div>
                  </footer><!-- .entry-footer -->
                  <?php endif; ?>
                  </article><!-- #post-<?php the_ID(); ?> -->
                </div>
              </div>
            </div>
          </section>
        </div>