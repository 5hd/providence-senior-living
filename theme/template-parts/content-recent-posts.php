   <?php

    $args = array(
      'posts_per_page'      => 3,
      'ignore_sticky_posts'   => 1,
      'post_status'         => 'publish',
      'order'         => 'DESC',
      'order_by'        => 'post_date',
    );
    
    $recent_posts = new WP_Query( $args );
    if ( $recent_posts->have_posts() ): while ( $recent_posts->have_posts() ): $recent_posts->the_post();
  ?>
                    
<h3 class="post-title">
<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
</h3>
<p class="post-date">
<?php echo get_the_date( 'l, F j, Y' ); ?>
</p>

                    <?php
    endwhile;
    wp_reset_postdata();
    endif;
  ?>