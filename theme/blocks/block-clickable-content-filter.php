<div class="content-switcher">
  <div class="container">
<div class="button-wrapper">
  <?php
    if (block_rows('filter')) :
    while (block_rows('filter')) :
    block_row('filter');
    ?>
  <div class="switcher-button <?php block_sub_field('extra-button-class'); ?>" data-filter=".<?php block_sub_field('title'); ?>">
    <img src="<?php block_sub_field('icon'); ?>"/>
    <h3 class="icon-title proportional-text"><?php block_sub_field('title'); ?></h3>
  </div>
  <?php endwhile;
    endif;
    reset_block_rows('filter');
    ?>
</div>
    <div class="filter-content-wrapper">
      <?php
    if (block_rows('filter')) :
    while (block_rows('filter')) :
    block_row('filter');
    ?>
  <div class="switcher-content-area <?php block_sub_field('title'); ?> <?php block_sub_field('extra-content-class'); ?>">
     <?php block_sub_field('content'); ?>
  </div>
  <?php endwhile;
    endif;
    reset_block_rows('filter');
    ?>
    </div>
  </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/3.0.6/isotope.pkgd.min.js"></script>
<script>
    jQuery(document).ready(function($) {


$(".button-wrapper .switcher-button").click ( function() {
    $(".button-wrapper .switcher-button").removeClass('filter-active');
    $(this).addClass('filter-active');

    var selectedFilter = $(this).data("filter");
    $(".content-wrapper").fadeTo(100, 0);
  
    $(".switcher-content-area").fadeOut().css('transform');

    setTimeout(function() {
      $(selectedFilter).fadeIn(100).css('transform');
      $(".content-wrapper").fadeTo(100, 1);
    }, 400);
  });
});
</script>