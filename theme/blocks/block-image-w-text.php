<?php
if (block_rows('row')) : ?>
<div class="story-component">
    <?php
    while (block_rows('row')) :
    block_row('row');
    $layout = '';
    if ( block_sub_value( 'reverse-layout' ) ) {
    $layout = ' reverse-layout';
    }
    ?>
    <div class="story-grid<?php echo $layout; ?>" style="opacity: 1; transform: translate3d(0px, 0px, 0px) scale3d(1, 1, 1) rotateX(0deg) rotateY(0deg) rotateZ(0deg) skew(0deg, 0deg); transform-style: preserve-3d;">
        <div class="story-content text-size-regular">
            <?php block_sub_field('copy'); ?>
            
            <?php if(block_sub_value('button-url')): ?>
            <a href="<?php block_sub_field('button-url'); ?>" class="button w-inline-block">
                <div class="button-text">
                    <div class="button-text-item"><?php block_sub_field('button-text'); ?></div>
                    <div class="button-text-item"><?php block_sub_field('button-text'); ?></div>
                </div>
            </a>
            <?php endif; ?>
        </div>
        <div class="story-image">
            <img
            src="<?php block_sub_field('image'); ?>"
            loading="lazy"
            sizes="(max-width: 991px) 92vw, 39vw"
            alt=""
            class="image-fill"
            />
        </div>
    </div>
    
    <?php endwhile;
    endif;
    reset_block_rows('row');
    ?>
</div>