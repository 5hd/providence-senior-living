<section class="checklist-section padding-vertical">
	<div class="page-padding">
		<div class="padding-huge">
			<div class="container-large">
				<h3 class="heading-medium text-color-white"><?php block_field('headline'); ?></h3>
				<div class="list-container text-size-regular">
					<?php
					if (block_rows('checklist')) : ?>
					<ul>
						<?php
						while (block_rows('checklist')) :
						block_row('checklist');
						?>
						<li><?php block_sub_field('list-item'); ?></li>
						<?php endwhile;
						endif;
						reset_block_rows('checklist');
						?>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>