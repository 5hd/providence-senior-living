
<section class="testimonial-carousel">
<div class="container">
<div class="swiper mySwiper">
  <div class="swiper-wrapper">
    <?php
    if (block_rows('testimonials')) :
    while (block_rows('testimonials')) :
    block_row('testimonials');
    ?>
    <div class="swiper-slide">
      <div class="testimonial-content-wrapper"><?php block_sub_field( 'testimonial' ); ?></div>
      <p class="testimonial-name"><?php block_sub_field( 'name' ); ?></p>
    </div>
    <?php endwhile;
    endif;
    reset_block_rows('testimonials');
    ?>
  </div>
  <div class="swiper-button-next"></div>
  <div class="swiper-button-prev"></div>
</div>
</div>
</section>

<style>

.swiper {
width: 100%;
height: 100%;
}
.swiper-slide {
padding: 20px;
text-align: center;
font-size: 18px;
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
}
.swiper-slide img {
display: block;
width: 100%;
height: 100%;
object-fit: cover;
}

.swiper-wrapper {
  align-items: center;
}

.swiper-button-next, .swiper-button-prev {
    color: #7f3d97;
}
</style>

<!-- Swiper JS -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
 <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
<!-- Initialize Swiper -->

<script>
var swiper = new Swiper(".mySwiper", {
navigation: {
nextEl: ".swiper-button-next",
prevEl: ".swiper-button-prev",
},
});
</script>