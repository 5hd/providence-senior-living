<!-- LOOP START -->

        <?php
        if (block_rows('accordion')) : ?>
        <section class="section-sticky-cards">
        <div class="hero-basic-trigger"></div>
        <div class="sticky-card">
        <?php

        $count = 1;
        while (block_rows('accordion')) :
        block_row('accordion');

        $class = '';
        $id = '';
        $data_id = '';
        if ( $count == 1 ) {
        $class = 'sticky-first-base';
        $id = 'one';
        $data_id = 'data-w-id="ec62843e-829b-6b5e-053b-d1893adc30c9';
    } else if ( $count == 2 ) {
        $class = 'sticky-second-base';
        $id = 'two';
    } else if ( $count == 3 ) {
        $class = 'sticky-third-base';
        $id = 'three';
    } else if ( $count == 4 ) {
        $class = 'sticky-fourth-base';
        $id = 'four';
    } else if ( $count == 5 ) {
        $class = 'sticky-fifth-base';
        $id = 'five';
    } else if ( $count == 6 ) {
        $class = 'sticky-sixth-base';
        $id = 'six';
    } else if ( $count == 7 ) {
        $class = 'sticky-seventh-base';
        $id = 'seven';
    } else if ( $count == 8 ) {
        $class = 'sticky-eighth-base';
        $id = 'eight';
    } else if ( $count == 9 ) {
        $class = 'sticky-ninth-base';
        $id = 'nine';
    } else if ( $count == 10 ) {
        $class = 'sticky-tenth-base';
        $id = 'ten';
    }

    $imageStyle ='';

    if ( block_sub_value( 'logo' ) ) {
    $imageStyle = ' logo';
}

        ?>

            <div id="<?php echo $id; ?>" class="sticky-anchor"></div>
            <div class="sticky-base <?php echo $class; ?> stacked">
            <div data-w-id="<?php echo $data_id; ?>" class="page-padding">
                <div class="sticky-cover">
                    <div class="container-large">
        <div class="sticky-card-item">
            <a href="/providence-senior-living" class="sticky-top w-inline-block">
                <div class="sticky-top-side">
                    <div class="text-size-large text-style-italic text-color-grey">
                        <?php block_sub_field('title'); ?>
                    </div>
                </div>
            </a>
            <div class="sticky-content">
                <div class="sticky-image" style="opacity: 1;">
                    <img
                    src="<?php block_sub_field('image'); ?>"
                    loading="lazy"
                    sizes="(max-width: 479px) 91vw, (max-width: 991px) 92vw, 43vw"
                    class="image-fill"
                    />
                </div>
                <div class="sticky-intro">
                    <div class="preview-title"><h2><?php block_sub_field('title'); ?></h2></div>
                    <div class="sticky-description">
                        <p class="text-size-regular"><?php block_sub_field('copy'); ?></p>
                    </div>

                    <?php if(block_sub_value('button-url')): ?>

                    <a href="<?php block_sub_field('button-url'); ?>" class="button w-inline-block">
                        <div class="button-text">
                            <div class="button-text-item"><?php block_sub_field('button-text'); ?></div>
                            <div class="button-text-item"><?php block_sub_field('button-text'); ?></div>
                        </div>
                    </a>

                <?php endif; ?>
                </div>
            </div>
        </div>
                </div>
                </div>
            </div>
        </div>
        
        <?php 

        $count++;
        endwhile;
        endif;
        reset_block_rows('team-member');
        ?>

        <!-- LOOP END -->

    </div>

</section>