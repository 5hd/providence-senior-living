<!-- <section class="services-accordion">
  <div class="card-wrapper">

  <?php
          if (block_rows('accordion')) :

              while (block_rows('accordion')) :
                  block_row('accordion');
          ?>  
             
                    <div class="sticky-card">
                    <div class="row">
                      <span><?php block_sub_field('title'); ?></span>
                      <div class="col-sm-12 col-md-6">
                      <div class="image">
                        <img src="<?php block_sub_field('main-image'); ?>" alt="" />
                      </div>
                      </div>
                      <div class="col-sm-12 col-md-6">
                      <div class="card-content">
                        <h2><?php block_sub_field('title'); ?></h2>
                        <?php block_sub_field('copy'); ?>
                      </div>
                      </div>
                    </div>
                    </div>
                 
          <?php endwhile;
          endif;

          reset_block_rows('accordion');
          ?>
        </div>
      </section> -->


<section class="section-sticky-cards">
    <div class="sticky-card">
        <div id="one" class="sticky-anchor"></div>
        <div class="sticky-base sticky-first-base stacked">
            <div data-w-id="ec62843e-829b-6b5e-053b-d1893adc30c9" class="page-padding">
                <div class="sticky-cover">
                    <div class="container-large">
                        <div class="sticky-card-item">
                            <a href="#one" class="sticky-top w-inline-block">
                                <div class="sticky-top-side"><div class="text-size-large text-style-italic text-color-grey">Our Difference</div></div>
                            </a>
                            <div class="sticky-content">
                                <div id="w-node-ec62843e-829b-6b5e-053b-d1893adc30d2-950d9604" class="sticky-image" style="opacity: 1;">
                                    <img
                                        src="https://assets.website-files.com/62b3589cd0f2573b462ee215/62b709439f4309349fb404d9_investment-slider-4.png"
                                        loading="lazy"
                                        srcset="
                                            https://assets.website-files.com/62b3589cd0f2573b462ee215/62b709439f4309349fb404d9_investment-slider-4-p-500.png 500w,
                                            https://assets.website-files.com/62b3589cd0f2573b462ee215/62b709439f4309349fb404d9_investment-slider-4-p-800.png 800w,
                                            https://assets.website-files.com/62b3589cd0f2573b462ee215/62b709439f4309349fb404d9_investment-slider-4.png       960w
                                        "
                                        sizes="(max-width: 479px) 91vw, (max-width: 991px) 92vw, 44vw"
                                        alt=""
                                        class="image-fill"
                                    />
                                </div>
                                <div
                                    id="w-node-ec62843e-829b-6b5e-053b-d1893adc30d4-950d9604"
                                    class="sticky-image-circle"
                                    style="transform: translate3d(0px, 0px, 0px) scale3d(1, 1, 1) rotateX(0deg) rotateY(0deg) rotateZ(0deg) skew(0deg, 0deg); transform-style: preserve-3d;"
                                >
                                    <img
                                        src="https://assets.website-files.com/62b3589cd0f2573b462ee215/62b709653b83088bbcab4b62_development-slider-1.jpg"
                                        loading="lazy"
                                        sizes="(max-width: 479px) 125.46000671386719px, (max-width: 991px) 201.95999145507812px, 201.9599609375px"
                                        srcset="
                                            https://assets.website-files.com/62b3589cd0f2573b462ee215/62b709653b83088bbcab4b62_development-slider-1-p-500.jpeg 500w,
                                            https://assets.website-files.com/62b3589cd0f2573b462ee215/62b709653b83088bbcab4b62_development-slider-1-p-800.jpeg 800w,
                                            https://assets.website-files.com/62b3589cd0f2573b462ee215/62b709653b83088bbcab4b62_development-slider-1.jpg        960w
                                        "
                                        alt=""
                                        class="image-fill"
                                    />
                                </div>
                                <div id="w-node-ec62843e-829b-6b5e-053b-d1893adc30d6-950d9604" class="sticky-intro">
                                    <div class="preview-title"><h2>Our Difference</h2></div>
                                    <div class="sticky-description"><p class="text-size-regular">We take senior living personally.</p></div>
                                    <a href="#" class="button w-inline-block">
                                        <div class="button-text">
                                            <div class="button-text-item">learn more</div>
                                            <div class="button-text-item">learn more</div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="two" class="sticky-anchor"></div>
        <div class="sticky-base sticky-second-base stacked">
            <div class="page-padding">
                <div class="sticky-cover">
                    <div class="container-large">
                        <div class="sticky-card-item">
                            <a href="/build" class="sticky-top w-inline-block">
                                <div class="sticky-top-side"><div class="text-size-large text-style-italic text-color-grey">New Construction</div></div>
                            </a>
                            <div class="sticky-content">
                                <div class="sticky-image" style="opacity: 1;">
                                    <img
                                        src="https://assets.website-files.com/62b3589cd0f2573b462ee215/62b7096f302e9a4a84481c02_developement-logo-tp-commons.jpg"
                                        loading="lazy"
                                        srcset="
                                            https://assets.website-files.com/62b3589cd0f2573b462ee215/62b7096f302e9a4a84481c02_developement-logo-tp-commons-p-500.jpeg   500w,
                                            https://assets.website-files.com/62b3589cd0f2573b462ee215/62b7096f302e9a4a84481c02_developement-logo-tp-commons-p-800.jpeg   800w,
                                            https://assets.website-files.com/62b3589cd0f2573b462ee215/62b7096f302e9a4a84481c02_developement-logo-tp-commons-p-1080.jpeg 1080w,
                                            https://assets.website-files.com/62b3589cd0f2573b462ee215/62b7096f302e9a4a84481c02_developement-logo-tp-commons.jpg         1600w
                                        "
                                        sizes="(max-width: 479px) 91vw, (max-width: 991px) 92vw, 44vw"
                                        alt=""
                                        class="image-fill"
                                    />
                                </div>
                                <div
                                    id="w-node-ec62843e-829b-6b5e-053b-d1893adc30f0-950d9604"
                                    class="sticky-image-circle"
                                    style="transform: translate3d(0px, 0px, 0px) scale3d(1, 1, 1) rotateX(0deg) rotateY(0deg) rotateZ(0deg) skew(0deg, 0deg); transform-style: preserve-3d;"
                                >
                                    <img
                                        src="https://assets.website-files.com/62b3589cd0f2573b462ee215/62b61ba335d5c283b4118867_investment-slider-1.png"
                                        loading="lazy"
                                        sizes="(max-width: 479px) 125.46000671386719px, (max-width: 991px) 201.95999145507812px, 201.9599609375px"
                                        srcset="
                                            https://assets.website-files.com/62b3589cd0f2573b462ee215/62b61ba335d5c283b4118867_investment-slider-1-p-500.png 500w,
                                            https://assets.website-files.com/62b3589cd0f2573b462ee215/62b61ba335d5c283b4118867_investment-slider-1-p-800.png 800w,
                                            https://assets.website-files.com/62b3589cd0f2573b462ee215/62b61ba335d5c283b4118867_investment-slider-1.png       960w
                                        "
                                        alt=""
                                        class="image-fill"
                                    />
                                </div>
                                <div class="sticky-intro">
                                    <div class="preview-title"><h2>New Construction</h2></div>
                                    <div class="sticky-description"><p class="text-size-regular">Forward-thinking senior living communities designed for sustainability and adaptability to future needs.</p></div>
                                    <a href="/build" class="button w-inline-block">
                                        <div class="button-text">
                                            <div class="button-text-item">learn more</div>
                                            <div class="button-text-item">learn more</div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="two" class="sticky-anchor"></div>
        <div class="sticky-base sticky-third-base stacked">
            <div class="page-padding">
                <div class="sticky-cover">
                    <div class="container-large">
                        <div class="sticky-card-item">
                            <a href="#two" class="sticky-top w-inline-block">
                                <div class="sticky-top-side"><div class="text-size-large text-style-italic text-color-grey">Adapting Existing Spaces</div></div>
                            </a>
                            <div class="sticky-content">
                                <div class="sticky-image" style="opacity: 1;">
                                    <img
                                        src="https://assets.website-files.com/62b3589cd0f2573b462ee215/62b61c93fb8d639148bcdfa0_Providence-Living-at-Maitland.jpg"
                                        loading="lazy"
                                        srcset="
                                            https://assets.website-files.com/62b3589cd0f2573b462ee215/62b61c93fb8d639148bcdfa0_Providence-Living-at-Maitland-p-500.jpeg   500w,
                                            https://assets.website-files.com/62b3589cd0f2573b462ee215/62b61c93fb8d639148bcdfa0_Providence-Living-at-Maitland-p-800.jpeg   800w,
                                            https://assets.website-files.com/62b3589cd0f2573b462ee215/62b61c93fb8d639148bcdfa0_Providence-Living-at-Maitland-p-1080.jpeg 1080w,
                                            https://assets.website-files.com/62b3589cd0f2573b462ee215/62b61c93fb8d639148bcdfa0_Providence-Living-at-Maitland-p-1600.jpeg 1600w,
                                            https://assets.website-files.com/62b3589cd0f2573b462ee215/62b61c93fb8d639148bcdfa0_Providence-Living-at-Maitland.jpg         1920w
                                        "
                                        sizes="(max-width: 479px) 91vw, (max-width: 991px) 92vw, 44vw"
                                        alt=""
                                        class="image-fill"
                                    />
                                </div>
                                <div
                                    id="w-node-ec62843e-829b-6b5e-053b-d1893adc310c-950d9604"
                                    class="sticky-image-circle"
                                    style="transform: translate3d(0px, 0px, 0px) scale3d(1, 1, 1) rotateX(0deg) rotateY(0deg) rotateZ(0deg) skew(0deg, 0deg); transform-style: preserve-3d;"
                                >
                                    <img
                                        src="https://assets.website-files.com/62b3589cd0f2573b462ee215/62b70444fb8d6354d3c25843_providence-independence-at-wildwood-6-source-27657.jpg"
                                        loading="lazy"
                                        sizes="(max-width: 479px) 125.46000671386719px, (max-width: 991px) 201.95999145507812px, 201.9599609375px"
                                        srcset="
                                            https://assets.website-files.com/62b3589cd0f2573b462ee215/62b70444fb8d6354d3c25843_providence-independence-at-wildwood-6-source-27657-p-500.jpeg  500w,
                                            https://assets.website-files.com/62b3589cd0f2573b462ee215/62b70444fb8d6354d3c25843_providence-independence-at-wildwood-6-source-27657-p-800.jpeg  800w,
                                            https://assets.website-files.com/62b3589cd0f2573b462ee215/62b70444fb8d6354d3c25843_providence-independence-at-wildwood-6-source-27657.jpg        1000w
                                        "
                                        alt=""
                                        class="image-fill"
                                    />
                                </div>
                                <div class="sticky-intro">
                                    <div class="preview-title"><h2>Adapting Existing Spaces</h2></div>
                                    <div class="sticky-description"><p class="text-size-regular">Retrofit and repurpose of existing spaces provides opportunities to connect with the local community.</p></div>
                                    <a href="#" class="button w-inline-block">
                                        <div class="button-text">
                                            <div class="button-text-item">learn more</div>
                                            <div class="button-text-item">learn more</div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="two" class="sticky-anchor"></div>
        <div class="sticky-base sticky-fourth-base stacked">
            <div class="page-padding">
                <div class="sticky-cover">
                    <div class="container-large">
                        <div class="sticky-card-item">
                            <a href="#two" class="sticky-top w-inline-block">
                                <div class="sticky-top-side"><div class="text-size-large text-style-italic text-color-grey">Management Services</div></div>
                            </a>
                            <div class="sticky-content">
                                <div class="sticky-image" style="opacity: 1;">
                                    <img
                                        src="https://assets.website-files.com/62b3589cd0f2573b462ee215/62b7094e302e9aa082481af1_investment-slider-3.png"
                                        loading="lazy"
                                        srcset="
                                            https://assets.website-files.com/62b3589cd0f2573b462ee215/62b7094e302e9aa082481af1_investment-slider-3-p-500.png 500w,
                                            https://assets.website-files.com/62b3589cd0f2573b462ee215/62b7094e302e9aa082481af1_investment-slider-3-p-800.png 800w,
                                            https://assets.website-files.com/62b3589cd0f2573b462ee215/62b7094e302e9aa082481af1_investment-slider-3.png       960w
                                        "
                                        sizes="(max-width: 479px) 91vw, (max-width: 991px) 92vw, 44vw"
                                        alt=""
                                        class="image-fill"
                                    />
                                </div>
                                <div
                                    id="w-node-ec62843e-829b-6b5e-053b-d1893adc3128-950d9604"
                                    class="sticky-image-circle"
                                    style="transform: translate3d(0px, 0px, 0px) scale3d(1, 1, 1) rotateX(0deg) rotateY(0deg) rotateZ(0deg) skew(0deg, 0deg); transform-style: preserve-3d;"
                                >
                                    <img
                                        src="https://assets.website-files.com/62b3589cd0f2573b462ee215/62b36c29f17ea13b3208e4cd_asset-slider-1.jpg"
                                        loading="lazy"
                                        sizes="(max-width: 479px) 125.46000671386719px, (max-width: 991px) 201.95999145507812px, 201.9599609375px"
                                        srcset="
                                            https://assets.website-files.com/62b3589cd0f2573b462ee215/62b36c29f17ea13b3208e4cd_asset-slider-1-p-500.jpeg 500w,
                                            https://assets.website-files.com/62b3589cd0f2573b462ee215/62b36c29f17ea13b3208e4cd_asset-slider-1-p-800.jpeg 800w,
                                            https://assets.website-files.com/62b3589cd0f2573b462ee215/62b36c29f17ea13b3208e4cd_asset-slider-1.jpg        960w
                                        "
                                        alt=""
                                        class="image-fill"
                                    />
                                </div>
                                <div class="sticky-intro">
                                    <div class="preview-title"><h2>Management Services</h2></div>
                                    <div class="sticky-description"><p class="text-size-regular">Our hands-on approach to management fosters engagement and commitment.</p></div>
                                    <a href="#" class="button w-inline-block">
                                        <div class="button-text">
                                            <div class="button-text-item" style="transform: translate3d(0px, 0%, 0px) scale3d(1, 1, 1) rotateX(0deg) rotateY(0deg) rotateZ(0deg) skew(0deg, 0deg); transform-style: preserve-3d;">
                                                learn more
                                            </div>
                                            <div class="button-text-item" style="transform: translate3d(0px, 0%, 0px) scale3d(1, 1, 1) rotateX(0deg) rotateY(0deg) rotateZ(0deg) skew(0deg, 0deg); transform-style: preserve-3d;">
                                                learn more
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="two" class="sticky-anchor"></div>
        <div class="sticky-base sticky-fifth-base stacked">
            <div class="page-padding">
                <div class="sticky-cover">
                    <div class="container-large">
                        <div class="sticky-card-item">
                            <a href="/our-communities" class="sticky-top w-inline-block">
                                <div class="sticky-top-side"><div class="text-size-large text-style-italic text-color-grey">Our Communities</div></div>
                            </a>
                            <div class="sticky-content">
                                <div class="sticky-image" style="opacity: 1;">
                                    <img
                                        src="https://assets.website-files.com/62b3589cd0f2573b462ee215/62b7095fddbeb8363c80c1bc_development-slider-2.png"
                                        loading="lazy"
                                        srcset="
                                            https://assets.website-files.com/62b3589cd0f2573b462ee215/62b7095fddbeb8363c80c1bc_development-slider-2-p-500.png 500w,
                                            https://assets.website-files.com/62b3589cd0f2573b462ee215/62b7095fddbeb8363c80c1bc_development-slider-2-p-800.png 800w,
                                            https://assets.website-files.com/62b3589cd0f2573b462ee215/62b7095fddbeb8363c80c1bc_development-slider-2.png       960w
                                        "
                                        sizes="(max-width: 479px) 91vw, (max-width: 991px) 92vw, 44vw"
                                        alt=""
                                        class="image-fill"
                                    />
                                </div>
                                <div
                                    id="w-node-ec62843e-829b-6b5e-053b-d1893adc3144-950d9604"
                                    class="sticky-image-circle"
                                    style="transform: translate3d(0px, 0px, 0px) scale3d(1, 1, 1) rotateX(0deg) rotateY(0deg) rotateZ(0deg) skew(0deg, 0deg); transform-style: preserve-3d;"
                                >
                                    <img
                                        src="https://assets.website-files.com/62b3589cd0f2573b462ee215/62b70957372fb3d80a5b267c_development-slider-3.jpg"
                                        loading="lazy"
                                        sizes="(max-width: 479px) 125.46000671386719px, (max-width: 991px) 201.95999145507812px, 201.9599609375px"
                                        srcset="
                                            https://assets.website-files.com/62b3589cd0f2573b462ee215/62b70957372fb3d80a5b267c_development-slider-3-p-500.jpeg 500w,
                                            https://assets.website-files.com/62b3589cd0f2573b462ee215/62b70957372fb3d80a5b267c_development-slider-3-p-800.jpeg 800w,
                                            https://assets.website-files.com/62b3589cd0f2573b462ee215/62b70957372fb3d80a5b267c_development-slider-3.jpg        960w
                                        "
                                        alt=""
                                        class="image-fill"
                                    />
                                </div>
                                <div class="sticky-intro">
                                    <div class="preview-title"><h2>Our Communities</h2></div>
                                    <div class="sticky-description"><p class="text-size-regular">Explore our innovative senior living communities.</p></div>
                                    <a href="/our-communities" class="button w-inline-block">
                                        <div class="button-text">
                                            <div class="button-text-item">learn more</div>
                                            <div class="button-text-item">learn more</div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
 