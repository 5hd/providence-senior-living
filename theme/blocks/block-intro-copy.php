<div class="section-hero-agenda">
    <section class="section-sticky" style="will-change: opacity; opacity: 0.89;">
        <div class="page-padding">
            <div id="w-node-b3fbf7a1-5f49-0468-36a9-7e9ac033e993-5d2ee22c" class="agenda-hero-component">
                <div class="section-page-title">
                    <h1 class="heading-large"><?php block_field( 'headline' ); ?></h1>
                </div>
            </div>
        </div>
    </section>
    <div class="hero-basic-trigger"></div>
    <section class="section-intro-columns">
        <div class="page-padding">
            <div class="padding-vertical padding-huge">
                <div class="container-large">
                    <?php echo block_value( 'additional-blocks' ); ?>
                </div>
            </div>
        </div>
    </section>
</div>