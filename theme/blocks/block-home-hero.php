<div class="section-features">
	<div class="video-container">
    <video src="<?php block_field( 'video' ); ?>" autoplay loop playsinline muted></video>
</div>
    <div class="hero-features w-container">
        <div class="w-layout-grid _4-col">
            <div class="home-feature small-caps" style="transform: translate3d(0px, 0px, 0px) scale3d(1, 1, 1) rotateX(0deg) rotateY(0deg) rotateZ(0deg) skew(0deg, 0deg); transform-style: preserve-3d;">Sustainable</div>
            <div class="home-feature small-caps" style="transform: translate3d(0px, 0px, 0px) scale3d(1, 1, 1) rotateX(0deg) rotateY(0deg) rotateZ(0deg) skew(0deg, 0deg); transform-style: preserve-3d;">Efficient</div>
            <div
                id="w-node-ec5ef94b-3bf8-4dd6-0db9-3ea6b7dfa378-2c2ee21a"
                class="home-feature small-caps"
                style="transform: translate3d(0px, 0px, 0px) scale3d(1, 1, 1) rotateX(0deg) rotateY(0deg) rotateZ(0deg) skew(0deg, 0deg); transform-style: preserve-3d;"
            >
                Functional
            </div>
            <div
                id="w-node-dff3df6e-9488-76b3-1ee5-3e41352e0296-2c2ee21a"
                class="home-feature small-caps last-feature"
                style="transform: translate3d(0px, 0px, 0px) scale3d(1, 1, 1) rotateX(0deg) rotateY(0deg) rotateZ(0deg) skew(0deg, 0deg); transform-style: preserve-3d;"
            >
                Cost effective
            </div>
        </div>
    </div>
</div>