<div class="story-component">
  <div class="story-grid" style="opacity: 1; transform: translate3d(0px, 0px, 0px) scale3d(1, 1, 1) rotateX(0deg) rotateY(0deg) rotateZ(0deg) skew(0deg, 0deg); transform-style: preserve-3d;">
    <div id="w-node-_4bf3e35f-c70c-ede5-5542-249b93f6521f-7bebb0fe" class="story-content">
      <?php block_field( 'copy' ); ?>
    </div>
    <div class="w-form">
      <form id="wf-form-Contact-Us-Form" name="wf-form-Contact-Us-Form" data-name="Contact Us Form" method="get" aria-label="Contact Us Form"><input type="text" class="text-field w-input" maxlength="256" name="name" data-name="Name" placeholder="Name" id="name" required=""><input type="text" class="text-field w-input" maxlength="256" name="Company" data-name="Company" placeholder="Company" id="Company"><input type="tel" class="text-field w-input" maxlength="256" name="Phone" data-name="Phone" placeholder="Phone" id="Phone" required=""><input type="email" class="text-field w-input" maxlength="256" name="Email" data-name="Email" placeholder="Email" id="Email-2" required=""><input type="text" class="text-field multi-line w-input" maxlength="256" name="Message" data-name="Message" placeholder="Message" id="Message"><input type="submit" value="Submit" data-wait="Please wait..." class="button w-button"></form>
      <div class="w-form-done" tabindex="-1" role="region" aria-label="Contact Us Form success">
        <div>Thank you! Your submission has been received!</div>
      </div>
      <div class="w-form-fail" tabindex="-1" role="region" aria-label="Contact Us Form failure">
        <div>Oops! Something went wrong while submitting the form.</div>
      </div>
    </div>
  </div>
</div>