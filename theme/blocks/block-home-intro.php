<div class="section-home-intro">
    <div class="padding-vertical padding-xhuge">
        <div class="_2-col-content w-container">
            <div
                data-w-id="9758a2fa-ec43-ae32-f862-cf126aa1dfb3"
                style="transform: translate3d(0px, 0px, 0px) scale3d(1, 1, 1) rotateX(0deg) rotateY(0deg) rotateZ(0deg) skew(0deg, 0deg); opacity: 1; transform-style: preserve-3d;"
                class="content-block">
                <div class="small-caps">Providence One Partners LLC</div>
                <h1 class="h1-intro"><span class="special">Leading </span>the <span class="special">Evolution</span><span class="special"> </span>of Real Estate Solutions.</h1>
                <p class="paragraph">
                    Providence One Partners is a multi-disciplined real estate development, investment, and brokerage company focused on client-driven real estate solutions. We are committed to incorporating sustainability into our
                    projects, recognizing our responsibility to our investors, to those who occupy our properties or live in our senior living communities, and to the environment around us.
                </p>
            </div>
            <div
                data-w-id="b45e44b7-89d4-2d9d-5e42-693294cf4cb2"
                style="transform: translate3d(0px, 0px, 0px) scale3d(1, 1, 1) rotateX(0deg) rotateY(0deg) rotateZ(0deg) skew(0deg, 0deg); opacity: 1; transform-style: preserve-3d;"
                class="round-top-image"
            >
                <img
                    src="/wp-content/uploads/2022/09/store-front-palms.png"
                    loading="lazy"
                    srcset="
                        https://assets.website-files.com/62b3589cd0f2573b462ee215/62b6e9b591de2d0b00674fd9_investment-slider-2-p-500.png 500w,
                        https://assets.website-files.com/62b3589cd0f2573b462ee215/62b6e9b591de2d0b00674fd9_investment-slider-2-p-800.png 800w,
                        https://assets.website-files.com/62b3589cd0f2573b462ee215/62b6e9b591de2d0b00674fd9_investment-slider-2.png       960w
                    "
                    sizes="(max-width: 991px) 92vw, 42vw"
                    alt=""
                    class="image-fill"
                />
            </div>
        </div>
    </div>
    <div class="page-padding"></div>
</div>
