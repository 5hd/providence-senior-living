<?php
$bg = '';
if ( block_value( 'purple-background' ) ) {
    $bg = 'style="background-color:#7f3d97;" ';
}
?>
<section <?php echo $bg; ?>class="section-banner">
    <div class="action-wrapper">
        <div class="page-padding">
            <div class="padding-vertical padding-xhuge">
                <div class="container-large">
                    <div class="action-intro">
                        <div id="w-node-c7d82394-8d00-b608-16cb-221d7965e125-7965e11c" class="banner-description padding-vertical">
                            <h3 class="heading-large">Get in touch</h3>
                            <a href="/contact/" class="button button-yellow w-inline-block">
                                <div class="button-text">
                                    <div class="button-text-item">get in touch</div>
                                    <div class="button-text-item">Get in touch</div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="preview-background">
        <div class="preview-paralax" style="will-change: transform; transform: translate3d(0px, 29.5669%, 0px) scale3d(1.1, 1.1, 1) rotateX(0deg) rotateY(0deg) rotateZ(0deg) skew(0deg, 0deg); transform-style: preserve-3d;">
            <div class="overlay overlay-light"></div>
        </div>
    </div>
</section>
