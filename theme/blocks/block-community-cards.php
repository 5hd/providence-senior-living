<section class="community-card-section">
    <div class="container">
        <div class="row justify-content-center">
            <?php
            if (block_rows('community')) :
            while (block_rows('community')) :
            block_row('community');
            ?>
            <div class="col-lg-4 m-4 location-cards">
                <div class="content-wrapper">
                    <div class="image" style="background-image:url('<?php block_sub_field( 'image' ); ?>')">
                        <a href="<?php block_sub_field('website_url'); ?>"></a>
                    </div>
                    <div class="title"><?php block_sub_field( 'title' ); ?></div>
                    <?php
                    $independentLiving = '';
                    $assistedLiving = '';
                    $memoryCare = '';
                    if(block_sub_value('independent-living')=== true){
                    $independentLiving = 'IL';
                    }
                    if(block_sub_value('assisted-living')=== true){
                    $assistedLiving = 'AL';
                    } 
                    if(block_sub_value('memory-care')=== true){
                    $memoryCare = 'MC';
                    } 
                    ?>
                    <div class="service-lines">
                    <?php if(block_sub_value('independent-living')): ?>
                    <p><?php echo $independentLiving ?></p>
                    <?php endif; ?>
                    <?php if(block_sub_value('assisted-living')): ?>
                    <p><?php echo $assistedLiving ?></p>
                    <?php endif; ?>
                    <?php if(block_sub_value('memory-care')): ?>
                    <p><?php echo $memoryCare ?></p>
                    <?php endif; ?>
                    </div>
                    <div class="line-break"></div>
                    <div class="info">
                        <p><?php block_sub_field('street_address'); ?></p>
                        <p><?php block_sub_field('city_state_zip'); ?></p>
                    </div>
                    <div class="line-break"></div>
                    <div class="links">
                        <a href="tel:<?php block_sub_field('phone_number'); ?>" class="email"><?php block_sub_field('phone_number'); ?></a>
                    </div>
                    <a type="button" class="button w-inline-block" href="<?php block_sub_field('website_url'); ?>" target="_blank">
                        Visit Website
                    </a>
                </div>
            </div>
            <?php endwhile;
            endif;
            reset_block_rows('community');
            ?>
        </div>
    </div>
</section>