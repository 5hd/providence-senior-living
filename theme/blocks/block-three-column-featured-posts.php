<?php

	// BACKGROUND COLOR
	$bg = '';

	if(block_value('background-color') == 'Blue'){
		$bg = 'bg-primary-custom';
	} else if(block_value('background-color') == 'Purple'){
		$bg = 'bg-secondary-custom';
	} else if(block_value('background-color') == 'Beige'){
		$bg = 'bg-tertiary-custom';
	}

	// MARGIN
	$margin = '';

	if(!block_value('margin-top')){
		$margin .= 'no-top-margin';
	}
	if(!block_value('margin-bottom')){
		$margin .= ' no-bottom-margin';
	}

	// OVERLAY COLOR
	$overlay = 'primary-fill';

?>

<div class="<?php echo $bg; ?>">
	<div class="container three-column-featured <?php echo $margin; ?>">
		<div class="row justify-content-center">
			<?php
		    if (block_rows('posts')) :

		        while (block_rows('posts')) :
		            block_row('posts');
		    ?>  
	         
	                <div class="block-wrapper">
	                	<div class="featured-block" style='background-image: url("<?php block_sub_field('image'); ?>")'>
	                		<?php
								if(block_sub_value('overlay-color') == 'Blue'){
									$overlay = 'primary-fill';
								} else if(block_sub_value('overlay-color') == 'Purple'){
									$overlay = 'secondary-fill';
								}	
							?>
							<div class="overlay <?php echo $overlay; ?>">
	                	</div>

	                	<h4><?php block_sub_field('heading'); ?></h4>
	                	<h5><?php block_sub_field('subhead'); ?></h5>
	                	
	                	<?php if(block_sub_value('button-url') && block_sub_value('button-label')): ?>

							
							<a href="<?php block_sub_field('button-url'); ?>">
								<button class="button">
									<?php block_sub_field('button-label'); ?>
								</button>
							</a>	
							

						<?php elseif(block_sub_value('button-url')): ?>

							<a href="<?php block_sub_field('button-url'); ?>">
								<button class="button">
									<?php block_sub_field('button-url'); ?>
								</button>
							</a>	

						<?php endif; ?>

	                </div>
	                <a class="featured-block-link" href="<?php block_sub_field('button-url'); ?>">
					</a>
	                </div>
		           
		    <?php endwhile;
		    endif;

		    reset_block_rows('posts');
		    ?>
		</div>
	</div>
</div>