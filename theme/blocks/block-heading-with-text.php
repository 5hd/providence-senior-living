<?php 
	
	// HEADING FORMAT
	$heading_format = 'h2';

	if(block_value('heading-format') == 'h2'){
		$heading_format = 'h2';	
	} else if(block_value('heading-format') == 'h3'){
		$heading_format = 'h3';
	} else if(block_value('heading-format') == 'h1'){
		$heading_format = 'h1';
	}

	// BACKGROUND COLOR
	$bg = '';

	if(block_value('background-color') == 'Beige'){
		$bg = 'bg-tertiary-custom';
	}

	// PADDING
	$padding = '';

	if(!block_value('padding-top')){
		$padding .= 'no-top-padding';
	}
	if(!block_value('padding-bottom')){
		$padding .= ' no-bottom-padding';
	}

	// BORDER
	$border = '';

	if(block_value('bottom-border')){
		$border = 'bottom-border';
	}

	// HTML Anchor
	$block_id = "";

	if(block_value('html-anchor')){
		$block_id = block_value('html-anchor');
	}

	//ANIMATION

	$animation = "";
	if(block_value('animated-headline')) {
		$animation = 'animated zoomIn duration2 eds-on-scroll';
	}

?>


<div id="<?php echo $block_id; ?>" class="<?php echo $bg; ?>">
	<div class="container heading-with-text no-top-margin no-bottom-margin <?php echo $padding; ?>">
		<div class="row align-items-end">
		<?php if(block_value('heading')): ?>
			<div class="col col-12 col-md-10">
				<<?php echo $heading_format; ?> class="<?php echo $animation; ?>"><?php block_field('heading'); ?></<?php echo $heading_format; ?>>
			</div>
			<?php endif; ?>
		<?php if(block_value('right-column-heading')): ?>
			<div class="col col-12 col-md-10">
				<<?php echo $heading_format; ?> class="<?php echo $animation; ?>"><?php block_field('right-column-heading'); ?></<?php echo $heading_format; ?>>
			</div>
			<?php endif; ?>

		</div>
		<div class="row">
			<?php if(!block_value('column-2')): ?>

				<div class="col col-12 col-md-10 text-container <?php echo $border; ?>">
					 <p><?php block_field('copy'); ?></p>
				</div>

			<?php elseif(block_value('copy') && block_value('column-2')): ?>

				<div class="col col-12 col-md-6 text-container">
					<p><?php echo block_field('copy'); ?></p>
				</div>

				<div class="col col-12 col-md-6 text-container">
					<p><?php echo block_field('column-2'); ?></p>
				</div>

			<?php endif; ?>
		</div>

		<?php if(block_value('button-url')): ?>
		<div class="row">
			<div class="col col-12 col-md-6">

				<?php if(block_value('secondary-button-url')): ?>

					
					<a href="<?php block_field('secondary-button-url'); ?>">
						<button class="button secondary-button">
							<?php block_field('secondary-button-label'); ?>
						</button>
					</a>	
					

				<?php endif; ?>

				<?php if(block_value('button-url') && block_value('button-label')): ?>

					
					<a href="<?php block_field('button-url'); ?>">
						<button class="button">
							<?php block_field('button-label'); ?>
						</button>
					</a>	

				<?php elseif(block_value('button-url')): ?>

					
					<a href="<?php block_field('button-url'); ?>">
						<button class="button">
							<?php block_field('button-url'); ?>
						</button>
					</a>	
					

				<?php endif; ?>
			</div>
		</div>
		<?php endif; ?>
		
	</div>
</div>
