<?php
if (block_value('intro-copy') ) :
?>
<section class="section-team">
    <div class="padding-bottom padding-xhuge">
        <div class="container-large">
            <div class="about-grid">
                <div id="w-node-_717467e8-017c-706a-622a-3c27693732e8-5d2ee22c" class="intro">
                    <div id="w-node-_717467e8-017c-706a-622a-3c27693732ef-5d2ee22c" class="about-description">
                        <div class="text-size-regular">
                            <?php block_field( 'intro-copy' ); ?>
                        </div>
                    </div>
                </div>
                <div id="w-node-_03320d21-6113-01ee-2d38-6a535f7a5f39-5d2ee22c" class="team-circles left">
                    <div id="w-node-_7fb84e8f-e657-401d-b2b6-13cfacb819a2-5d2ee22c" class="small-circle">
                        <img
                        src="<?php block_field( 'image-1' ); ?>"
                        loading="lazy"
                        sizes="(max-width: 479px) 35vw, (max-width: 767px) 97.91999816894531px, (max-width: 991px) 15vw, 12vw"
                        class="image-fill"
                        />
                    </div>
                    <div id="w-node-_7fb84e8f-e657-401d-b2b6-13cfacb819a2-5d2ee22c" class="small-circle">
                        <img
                        src="<?php block_field( 'image-2' ); ?>"
                        loading="lazy"
                        sizes="(max-width: 479px) 35vw, (max-width: 767px) 97.91999816894531px, (max-width: 991px) 15vw, 12vw"
                        class="image-fill"
                        />
                    </div>
                    <div id="w-node-_7fb84e8f-e657-401d-b2b6-13cfacb819a2-5d2ee22c" class="small-circle">
                        <img
                        src="<?php block_field( 'image-3' ); ?>"
                        loading="lazy"
                        sizes="(max-width: 479px) 35vw, (max-width: 767px) 97.91999816894531px, (max-width: 991px) 15vw, 12vw"
                        class="image-fill"
                        />
                    </div>
                    <div id="w-node-_7fb84e8f-e657-401d-b2b6-13cfacb819a2-5d2ee22c" class="small-circle">
                        <img
                        src="<?php block_field( 'image-4' ); ?>"
                        loading="lazy"
                        sizes="(max-width: 479px) 35vw, (max-width: 767px) 97.91999816894531px, (max-width: 991px) 15vw, 12vw"
                        class="image-fill"
                        />
                    </div>
                </div>
                <div id="w-node-_1c84043f-3372-7c76-e2b5-f673b50a5429-5d2ee22c" class="team-circles right">
                    <div id="w-node-_1c84043f-3372-7c76-e2b5-f673b50a542c-5d2ee22c" class="small-circle">
                        <img
                        src="<?php block_field( 'image-5' ); ?>"
                        loading="lazy"
                        sizes="(max-width: 479px) 35vw, (max-width: 767px) 97.91999816894531px, (max-width: 991px) 15vw, 12vw"
                        class="image-fill"
                        />
                    </div>
                    <div id="w-node-_1c84043f-3372-7c76-e2b5-f673b50a542c-5d2ee22c" class="small-circle">
                        <img
                        src="<?php block_field( 'image-6' ); ?>"
                        loading="lazy"
                        sizes="(max-width: 479px) 35vw, (max-width: 767px) 97.91999816894531px, (max-width: 991px) 15vw, 12vw"
                        class="image-fill"
                        />
                    </div>
                    <div id="w-node-_1c84043f-3372-7c76-e2b5-f673b50a542c-5d2ee22c" class="small-circle">
                        <img
                        src="<?php block_field( 'image-7' ); ?>"
                        loading="lazy"
                        sizes="(max-width: 479px) 35vw, (max-width: 767px) 97.91999816894531px, (max-width: 991px) 15vw, 12vw"
                        class="image-fill"
                        />
                    </div>
                    <div id="w-node-_1c84043f-3372-7c76-e2b5-f673b50a542c-5d2ee22c" class="small-circle">
                        <img
                        src="<?php block_field( 'image-8' ); ?>"
                        loading="lazy"
                        sizes="(max-width: 479px) 35vw, (max-width: 767px) 97.91999816894531px, (max-width: 991px) 15vw, 12vw"
                        class="image-fill"
                        />
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>
<?php
if (block_rows('team-member')) : ?>
<div class="story-component container-medium">
    <?php
    while (block_rows('team-member')) :
    block_row('team-member');
    ?>
    <div class="bio-container">
        <div id="w-node-_0f54d9e3-e94b-66a0-435f-a415100cbd21-5d2ee22c" class="bio-image">
            <img src="<?php block_sub_field('headshot'); ?>" loading="lazy" alt="" class="image-fill" />
        </div>
        <div id="w-node-_4bf3e35f-c70c-ede5-5542-249b93f6521f-5d2ee22c" class="bio-content">
            <h2 class="heading-small"><span class="special blue"><?php block_sub_field('name'); ?>,</span> <?php block_sub_field('title'); ?>, <?php block_sub_field('affiliation'); ?></h2>
            <div class="text-size-regular">
                <?php block_sub_field('bio'); ?>
            </div>
        </div>
    </div>
    
    <?php endwhile;
    endif;
    reset_block_rows('team-member');
    ?>
</div>