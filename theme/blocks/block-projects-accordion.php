<?php
    $category = '';
    if(block_value('category') == 'Commercial Development') {
    $category = 'Commercial Development';
    } else if(block_value('category') == 'Senior Living Investments') {
    $category = 'Senior Living Investments';
    } else if(block_value('category') == 'Senior Living Communities') {
    $category = 'Senior Living Communities';
    }
?>

<section class="section-projects-list padding-vertical">
    <div class="page-padding">
        <div class="padding-vertical padding-huge">
            <div class="container-large">
                <div class="agenda-day-title">
                    <div id="w-node-e46d8022-2249-974c-13b1-c116e8c2c720-4b4c246a" class="agenda-sidebar">
                        <div class="sidebar-title">
                            <h3 class="heading-medium text-color-white"><?php echo $category; ?> <span class="special green">projects</span></h3>
                        </div>
                    </div>
                    <div id="w-node-e46d8022-2249-974c-13b1-c116e8c2c731-4b4c246a" class="agenda-grid">
                        <div id="one" class="agenda-card-wrapper">
                            <div class="agenda-cards">

                                <!-- LOOP -->

                                <?php
                                    $args = array(
                                      'post_type'      => 'projects',
                                      'category_name'     => $category,
                                      'posts_per_page'      => 100,
                                      'ignore_sticky_posts'   => 1,
                                      'post_status'         => 'publish',
                                      'order'         => 'DESC',
                                      'order_by'        => 'post_date',
                                    );
                                ?>

                                <?php
                $projects = new WP_Query( $args );
                if ( $projects->have_posts() ): while ( $projects->have_posts() ): $projects->the_post();
            ?>

                                <div class="agenda-item">
                                    <div class="agenda-top">
                                        <div class="agenda-icon">
                                            <img
                                            src="https://assets.website-files.com/62b3589cd0f2573b462ee215/62b3589dd0f25795ae2ee2ac_chevron-down---filled(24x24)%402x%20(2).svg"
                                            loading="lazy"
                                            class="agenda-icon-image"
                                            style="transform: translate3d(0px, 0px, 0px) scale3d(1, 1, 1) rotateX(0deg) rotateY(0deg) rotateZ(0deg) skew(0deg, 0deg); transform-style: preserve-3d;"
                                            />
                                        </div>
                                        <div class="agenda-top-title">
                                            <div class="agenda-content">
                                                <div class="text-size-large">
                                                    <strong><?php the_title(); ?></strong>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="agenda-bottom" style="height: 0px;">
                                        <div class="agenda-bottom-content" style="transform: translate3d(0px, -2rem, 0px) scale3d(1, 1, 1) rotateX(0deg) rotateY(0deg) rotateZ(0deg) skew(0deg, 0deg); transform-style: preserve-3d;">
                                            <div id="w-node-b542e14d-0b0d-e5ff-d886-a78783a763a2-4b4c246a" class="text-content">
                                                <div class="text-size-regular">
                                                    <?php the_excerpt(); ?>
                                                </div>
                                            </div>
                                            <div class="project-image" style="opacity: 0;">
                                                <?php the_post_thumbnail(); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <?php
                                endwhile;
                                wp_reset_postdata();
                                endif;
                                ?>

                                <!-- END LOOP -->


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>