<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package fivehdstarter
 */

?>

  </div><!-- #content -->

  <footer id="colophon" class="site-footer">
    <div class="container no-top-margin no-bottom-margin">
      <div class="row footer-menus">
        <div class="col col-12 col-lg-3 footer-blurb">
          <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
            <img src="<?php echo get_theme_mod('footer-logo'); ?>" />
          </a>
          <p><?php echo get_theme_mod('paragraph'); ?></p>

          <?php if(get_theme_mod('policy-text')): ?>

          <a href="<?php echo get_theme_mod('policy-link'); ?>" target="_blank"><?php echo get_theme_mod('policy-text'); ?></a>

        <?php endif; ?>

        <div class="equal-housing-logo">
            <?php echo file_get_contents( get_template_directory() . '/img/equal-housing-3.svg' ); ?>
          </div>

        </div>
        <div class="col col-lg-9">
          <?php
            wp_nav_menu( array(
              'theme_location'  => 'footer-1-navigation',
              'items_wrap'      => '<ul class="navbar-nav me-auto mb-lg-0 justify-content-center">%3$s</ul>',
            ) );
          ?>
        </div>
      </div>
      <div class="row">
        <div class="col col-12 col-sm-12 col-md-4">
          <p>&copy; <?php echo date ('Y'); ?> All rights reserved</p>
        </div>
        <div class="col col-12 col-sm-12 col-md-8 footer-social">          
          <div class="social-icons-footer">
          <?php if(get_theme_mod('facebook_url')): ?>
            <!-- FACEBOOK -->
            <a href="<?php echo get_theme_mod('facebook_url'); ?>" target="_blank">
              <svg fill="#000000" xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 64 64" width="64px" height="64px"><path d="M48,7H16c-4.418,0-8,3.582-8,8v32c0,4.418,3.582,8,8,8h17V38h-6v-7h6v-5c0-7,4-11,10-11c3.133,0,5,1,5,1v6h-4 c-2.86,0-4,2.093-4,4v5h7l-1,7h-6v17h8c4.418,0,8-3.582,8-8V15C56,10.582,52.418,7,48,7z"/></svg>
            </a>
          <?php endif; ?>
          <?php if(get_theme_mod('linkedin_url')): ?>
            <!-- LINKEDIN -->
            <a href="<?php echo get_theme_mod('linkedin_url'); ?>" target="_blank">
              <svg fill="#000000" xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 50 50" width="50px" height="50px">    <path d="M41,4H9C6.24,4,4,6.24,4,9v32c0,2.76,2.24,5,5,5h32c2.76,0,5-2.24,5-5V9C46,6.24,43.76,4,41,4z M17,20v19h-6V20H17z M11,14.47c0-1.4,1.2-2.47,3-2.47s2.93,1.07,3,2.47c0,1.4-1.12,2.53-3,2.53C12.2,17,11,15.87,11,14.47z M39,39h-6c0,0,0-9.26,0-10 c0-2-1-4-3.5-4.04h-0.08C27,24.96,26,27.02,26,29c0,0.91,0,10,0,10h-6V20h6v2.56c0,0,1.93-2.56,5.81-2.56 c3.97,0,7.19,2.73,7.19,8.26V39z"/></svg>
            </a>
          <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
  </footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
<script>

  $("p:contains('Creating the Best Day Possible™')").html(function(_, html) {
   return html.replace(/(Creating the Best Day Possible™)/g, '<span class="special blue">$1</span>');
});
</script>

</body>
</html>
