<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package fivehdstarter
 */

?>
<!doctype html>
<html <?php language_attributes(); ?> data-wf-page="62b9be51c076150f950d9604" data-wf-site="62b3589cd0f2573b462ee215">
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

  <header id="masthead" class="site-header">


        <div class="sticky-nav page-padding" id="top-banner">
      <div class="container-large no-top-margin no-bottom-margin">
        <nav class="navbar navbar-expand-lg navbar-light">
          <div class="site-branding">
            <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo get_theme_mod('header-logo'); ?>" /></a>
          </div>

          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          
          <?php

            wp_nav_menu( array(
              'theme_location'  => 'main-navigation',
              'depth'           => 2, // 1 = no dropdowns, 2 = with dropdowns.
              'container'       => 'div',
              'container_class' => 'collapse navbar-collapse',
              'container_id'    => 'navbarNavDropdown',
              'menu_class'      => 'navbar-nav mr-auto',
              'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
              'walker'          => new WP_Bootstrap_Navwalker(),
          ) );
          ?>

          <div class="header-button">
            <div id="w-node-_17edb690-c5d1-181a-b556-ed5395cf6c9c-95cf6c81" class="nav-right"><a href="tel:407-333-0900" class="button w-inline-block"><div class="button-text"><div class="button-text-item">(407) 333-0900</div><div class="button-text-item">call 407.333.0900</div></div></a></div>
<div id="w-node-_17edb690-c5d1-181a-b556-ed5395cf6ca3-95cf6c81" class="menu-button w-nav-button" style="-webkit-user-select: text;" aria-label="menu" role="button" tabindex="0" aria-controls="w-nav-overlay-0" aria-haspopup="menu" aria-expanded="false"><div class="menu-icon"><img src="https://assets.website-files.com/62b3589cd0f2573b462ee215/62b3589dd0f2574baf2ee2ab_menu-2(24x24)%402x.svg" loading="lazy" alt=""></div></div>
          </div>
          
        </nav>
      </div>
    </div>



<!-- <div data-animation="default" data-collapse="medium" data-duration="400" data-easing="ease" data-easing2="ease-out-cubic" role="banner" class="main-navbar w-nav"><div class="page-padding"><div class="container-large"><div class="nav-wrapper"><a href="/providence-senior-living" id="w-node-_17edb690-c5d1-181a-b556-ed5395cf6c85-95cf6c81" aria-current="page" class="nav-brand w-nav-brand w--current"><img src="/wp-content/uploads/2024/05/62b61a313d21d07d2b692510_Providence-Senior-Living-Logo-notag.svg" loading="lazy" width="213" alt="" class="image"></a><nav role="navigation" id="w-node-_17edb690-c5d1-181a-b556-ed5395cf6c87-95cf6c81" class="nav-left w-nav-menu"><div class="nav-link-wrapper"><a href="/providence-senior-living/our-difference" class="nav-link senior-living w-inline-block"><div>Our Difference</div></a><a href="/providence-senior-living/build" class="nav-link senior-living w-inline-block"><div>Build</div></a><a href="/providence-senior-living/investments-acquisitions" class="nav-link senior-living w-inline-block"><div>Investments &amp; Acquisitions</div></a><a href="/providence-senior-living/management" class="nav-link senior-living w-inline-block"><div>Manage</div></a><a href="/providence-senior-living/senior-living-communities" class="nav-link senior-living w-inline-block"><div>Our Communities</div></a><a href="/providence-senior-living/about-providence-senior-living" class="nav-link senior-living w-inline-block"><div>About</div></a></div></nav>

<div id="w-node-_17edb690-c5d1-181a-b556-ed5395cf6c9c-95cf6c81" class="nav-right"><a href="tel:407-333-0900" class="button w-inline-block"><div class="button-text"><div class="button-text-item">407.333.0900</div><div class="button-text-item">call 407.333.0900</div></div></a></div>
<div id="w-node-_17edb690-c5d1-181a-b556-ed5395cf6ca3-95cf6c81" class="menu-button w-nav-button" style="-webkit-user-select: text;" aria-label="menu" role="button" tabindex="0" aria-controls="w-nav-overlay-0" aria-haspopup="menu" aria-expanded="false"><div class="menu-icon"><img src="https://assets.website-files.com/62b3589cd0f2573b462ee215/62b3589dd0f2574baf2ee2ab_menu-2(24x24)%402x.svg" loading="lazy" alt=""></div></div>

</div></div></div><div class="w-nav-overlay" data-wf-ignore="" id="w-nav-overlay-0"></div></div> -->

  </header><!-- #masthead -->

  <div id="content" class="site-content">
