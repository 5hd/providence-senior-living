<?php
/**
 * The header for Providence One Pages
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package fivehdstarter
 */

?>
<!doctype html>
<html <?php language_attributes(); ?> data-wf-page="62b9be51c076150f950d9604" data-wf-site="62b3589cd0f2573b462ee215">
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

  <header id="masthead" class="site-header">
<div data-animation="default" data-collapse="medium" data-duration="400" data-easing="ease" data-easing2="ease-out-cubic" role="banner" class="main-navbar w-nav"><div class="page-padding"><div class="container-large"><div class="nav-wrapper"><a href="/" id="w-node-_38a87566-d4ad-de39-9144-52828fb72620-8fb72609" aria-current="page" class="nav-brand w-nav-brand w--current" aria-label="home"><img src="/wp-content/uploads/2022/09/62b3608447540f686296ffa7_providence-one-logo.png" loading="lazy" alt="" class="image"></a><nav role="navigation" id="w-node-_38a87566-d4ad-de39-9144-52828fb7260d-8fb72609" class="nav-left w-nav-menu"><div class="nav-link-wrapper"><a href="about-providence-one-partners/" class="nav-link w-inline-block"><div>About</div></a><a href="/providence-senior-living" class="nav-link w-inline-block"><div>Providence Senior Living</div></a><a href="/commercial-real-estate" class="nav-link w-inline-block"><div>Commercial Real Estate</div></a><div data-hover="false" data-delay="0" id="w-node-_27c9502b-c27e-87ab-14ed-2ee33bdac931-8fb72609" class="w-dropdown"><div class="w-dropdown-toggle" id="w-dropdown-toggle-0" aria-controls="w-dropdown-list-0" aria-haspopup="menu" aria-expanded="false" role="button" tabindex="0"><div class="w-icon-dropdown-toggle" aria-hidden="true"></div><a href="/contact"><div class="nav-link">Contact Us</div></a></div><nav class="dropdown-list w-dropdown-list" id="w-dropdown-list-0" aria-labelledby="w-dropdown-toggle-0"><a href="http://www.providence-one.com" target="_blank" class="nav-link dropdown w-dropdown-link" tabindex="0">Careers</a></nav></div></div></nav><div id="w-node-_38a87566-d4ad-de39-9144-52828fb72625-8fb72609" class="nav-right"><a href="tel:407-333-0900" class="button w-inline-block"><div class="button-text"><div class="button-text-item">407.333.0900</div><div class="button-text-item">call 407.333.0900</div></div></a></div><div id="w-node-_38a87566-d4ad-de39-9144-52828fb72623-8fb72609" class="menu-button w-nav-button" style="-webkit-user-select: text;" aria-label="menu" role="button" tabindex="0" aria-controls="w-nav-overlay-0" aria-haspopup="menu" aria-expanded="false"><div class="menu-icon"><img src="https://assets.website-files.com/62b3589cd0f2573b462ee215/62b3589dd0f2574baf2ee2ab_menu-2(24x24)%402x.svg" loading="lazy" alt=""></div></div></div></div></div><div class="w-nav-overlay" data-wf-ignore="" id="w-nav-overlay-0"></div></div>

  </header><!-- #masthead -->

  <div id="content" class="site-content">
