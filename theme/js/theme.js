jQuery(document).ready(function() { 
        jQuery('.dropdown-toggle.nav-link').click(function(e) {
        // console.log("window width: " + jQuery(window).width());
        
        // Check for desktop
        if (jQuery(window).width() < 992) {
            
            // Check for expanded
            if(jQuery(this).hasClass('expanded')){
                // console.log("it's open");
            } else {
                e.preventDefault();    

                // Clear other open menus
                jQuery('.expanded').removeClass('expanded');
                jQuery('ul.show').removeClass('show');

                jQuery(this).addClass('expanded');
                jQuery(this).siblings('ul').addClass("show");
            }
        }
        // alert('You clicked the link...');
    });

    $banner_height = jQuery("#top-banner").height();

    // if(jQuery("#wpadminbar")){
    //     // console.log("Howdy admin");
    //     $banner_height += jQuery("#wpadminbar").height();
    // }

    console.log("banner_height: " + $banner_height);

    jQuery(window).scroll(function() {

        console.log(jQuery(window).scrollTop());

        if(!jQuery(".sticky-nav").hasClass("stuck")){
            if(jQuery(window).scrollTop() >= $banner_height){
                jQuery(".sticky-nav").addClass("stuck");
            }
        } else if(jQuery(window).scrollTop() < $banner_height) {
            jQuery(".sticky-nav").removeClass("stuck");
        }

    });


});