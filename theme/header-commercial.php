<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package fivehdstarter
 */

?>
<!doctype html>
<html <?php language_attributes(); ?> data-wf-page="62b9be51c076150f950d9604" data-wf-site="62b3589cd0f2573b462ee215">
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

  <header id="masthead" class="site-header">
<div data-animation="default" data-collapse="medium" data-duration="400" data-easing="ease" data-easing2="ease-out-cubic" role="banner" class="main-navbar w-nav"><div class="page-padding"><div class="container-large"><div class="nav-wrapper"><a href="/commercial-real-estate" id="w-node-_30f37476-0f9f-c980-00d7-b3e7874e52b7-874e52b3" aria-current="page" class="nav-brand w-nav-brand w--current"><img src="https://assets.website-files.com/62b3589cd0f2573b462ee215/62d82ad57117c51236f21b20_Providence-CRE-placeholder.svg" loading="lazy" width="213" height="Auto" alt="" class="image"></a><nav role="navigation" id="w-node-_30f37476-0f9f-c980-00d7-b3e7874e52b9-874e52b3" class="nav-left w-nav-menu"><div class="nav-link-wrapper"><a href="/commercial-real-estate/commercial-development" class="nav-link real-state w-inline-block"><div>Commercial Development</div></a><a href="/commercial-real-estate/investment" class="nav-link real-state w-inline-block"><div>Investment</div></a><a href="/commercial-real-estate/consulting" class="nav-link real-state w-inline-block"><div>Consulting</div></a><a href="/commercial-real-estate/asset-management" class="nav-link real-state w-inline-block"><div>Asset Management</div></a><a href="/commercial-real-estate/partnerships" class="nav-link real-state w-inline-block"><div>Partnerships</div></a><div class="nav-responsive show-tablet"></div></div></nav><div id="w-node-_30f37476-0f9f-c980-00d7-b3e7874e52d1-874e52b3" class="nav-right"><a href="tel:407-333-0900" class="button w-inline-block"><div class="button-text"><div class="button-text-item">407.333.0900</div><div class="button-text-item">call 407.333.0900</div></div></a></div><div id="w-node-_30f37476-0f9f-c980-00d7-b3e7874e52d8-874e52b3" class="menu-button w-nav-button" style="-webkit-user-select: text;" aria-label="menu" role="button" tabindex="0" aria-controls="w-nav-overlay-0" aria-haspopup="menu" aria-expanded="false"><div class="menu-icon"><img src="https://assets.website-files.com/62b3589cd0f2573b462ee215/62b3589dd0f2574baf2ee2ab_menu-2(24x24)%402x.svg" loading="lazy" alt=""></div></div></div></div></div><div class="w-nav-overlay" data-wf-ignore="" id="w-nav-overlay-0"></div></div>

  </header><!-- #masthead -->

  <div id="content" class="site-content">
